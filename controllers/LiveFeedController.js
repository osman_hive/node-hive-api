const liveData = require("../models/LiveData")

module.exports = {
    liveHomeFeed: async function(req, res) {
       const ld = liveData.lData

       const homeFeed = await ld.aggregate([
            { 
                $project: {
                    broadcast_id: '$broadcast_id',
                    username: '$username',
                    owner_id: '$owner_id',
                    status: '$status',
                    gender: '$gender',
                    country: '$country',
                    weight: { 
                        '$or': [
                            { '$eq': ['$status', 'live'] }, 
                            { '$eq': ['$gender', 'female'] }
                        ]
                    }
                }
            },
            { 
                $group: {
                    _id: { 
                        owner_id: '$owner_id' 
                    }
                }
            },
            { 
                $sort: { 
                    weight: -1,
                    broadcast_id: -1 
                } 
            },
            { 
                $skip: 0
            },
            { 
                $limit: 10
            }
        ])
        .exec();
        
       res.send(homeFeed)
    }
}

