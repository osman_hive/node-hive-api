const express = require("express")
const app = express()
const dbCon = require("./config/db.js")
const LiveFeedRouter = require("./routes/LiveFeedRouter")

app.use("/broadcast", LiveFeedRouter)

app.listen(3000, function() {
    console.log("server listening on port 3000")
})