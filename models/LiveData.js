const dbCon = require("../config/db")
const mongoose = require('mongoose')

const liveData = mongoose.model('liveData', mongoose.Schema({
        broadcast_id: Number,
        username: String,
        owner_id: Number,
        status: {
            type: String,
            enum: [ 'live', 'stop', 'ban' ]
        },
        gender: {
            type: String,
            enum: [ 'male', 'female' ]
        },
        country: String
    })
)

// const homeFeed = yield liveData.aggregate([
//     { 
//         $project: {
//             broadcast_id: '$broadcast_id',
//             username: '$username',
//             owner_id: '$owner_id',
//             status: '$status',
//             gender: '$gender',
//             country: '$country',
//             weight: { 
//                 '$or': [
//                     { '$eq': [{'$status': 'live'}] }, 
//                     { '$eq': [{'$gender': 'female'}] }
//                 ]
//             }
//         }
//     },
//     { 
//         $group: {
//             _id: { 
//                 owner_id: '$owner_id' 
//             }
//         }
//     },
//     { 
//         $sort: { 
//             weight: -1,
//             broadcast_id: -1 
//         } 
//     },
//     { 
//         $skip: 0
//     },
//     { 
//         $limit: 10
//     }
// ])
// .exec();

exports.lData = liveData
// exports.homeFeed = homeFeed