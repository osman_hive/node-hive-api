const express = require("express")
const router = express.Router()
const LiveDataController = require("../controllers/LiveFeedController")

router.get("/home", LiveDataController.liveHomeFeed)

module.exports = router